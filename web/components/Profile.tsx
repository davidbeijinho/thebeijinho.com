import React from "react";
import Card from "components/Card/Card";
import RightImage from "components/RightImage";

const Profile: React.FunctionComponent = () => {
  return (
    <div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
      <Card />
      <RightImage />
    </div>
  );
};

export default Profile;
