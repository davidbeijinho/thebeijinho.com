import React from "react";

const RightImage: React.FunctionComponent = () => {
  return (
    <div className="w-full lg:w-2/5">
      <img
        alt="David Beijinho"
        src="/images/me.webp"
        className="rounded-none lg:rounded-lg shadow-2xl hidden lg:block"
      />
    </div>
  );
};

export default RightImage;
