import React from "react";
import classNames from "classnames";

interface Props {
  className: string;
  children: React.ReactNode;
}

const CardLine: React.FunctionComponent<Props> = ({
  className = "",
  children,
}) => {
  return (
    <p
      className={classNames(
        "flex items-center justify-center lg:justify-start",
        className
      )}
    >
      {children}
    </p>
  );
};

export default CardLine;
