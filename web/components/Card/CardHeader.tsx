import React from "react";

const CardHeader: React.FunctionComponent = () => {
  return (
    <>
      <h1 className="text-3xl font-bold pt-8 lg:pt-0">David Beijinho</h1>
      {/* TODO can this be wrapped ? */}
      <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25" />
    </>
  );
};

export default CardHeader;
