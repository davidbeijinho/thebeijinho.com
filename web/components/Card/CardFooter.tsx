import Link from "components/Link";
import React from "react";

const CardFooter: React.FunctionComponent = () => {
  return (
    <div className="mt-6 pb-16 lg:pb-0 w-3/5 lg:w-2/5 mx-auto flex flex-wrap items-center justify-between">
      <Link href="mailto:davidbeijinho@gmail.com">
        <img
          alt="Email link"
          className="w-8 hover:opacity-25"
          src="/images/icons/bx-mail-send.svg"
        />
      </Link>
      <Link href="/files/David_Beijinho_CV.pdf" target="_blank">
        <img
          alt="Download Resume"
          className="w-8 hover:opacity-25"
          src="/images/icons/bxs-file.svg"
        />
      </Link>
      <Link href="https://github.com/davidbeijinho/" target="_blank">
        <img
          alt="Link to Github"
          className="w-8 hover:opacity-25"
          src="/images/icons/bxl-github.svg"
        />
      </Link>
      <Link href="https://gitlab.com/davidbeijinho/" target="_blank">
        <img
          alt="Link to Github"
          className="w-8 hover:opacity-25"
          src="/images/icons/bxl-gitlab.svg"
        />
      </Link>
    </div>
  );
};

export default CardFooter;
