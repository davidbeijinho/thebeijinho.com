import React from "react";
import RoundImage from "components/RoundImage";
import CardHeader from "components/Card/CardHeader";
import CardBody from "components/Card/CardBody";
import CardFooter from "components/Card/CardFooter";

const Profile: React.FunctionComponent = () => {
  return (
    <div className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0">
      <div className="p-4 md:p-12 text-center lg:text-left">
        <RoundImage />
        <CardHeader />
        <CardBody />
        <CardFooter />
      </div>
    </div>
  );
};

export default Profile;
