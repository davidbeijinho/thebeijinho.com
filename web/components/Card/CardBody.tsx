import React from "react";
import CardLine from "./CardLine";

const CardBody: React.FunctionComponent = () => {
  return (
    <>
      <CardLine className="pt-4 text-base font-bold">Developper</CardLine>
      <p className="pt-8 text-sm">
        {`Hello, I am a Frontend Developer, and I like to develop for different
        media and interactivity experiences.Recently I have been developing my
        knowledge about the dev-ops approach and really like it how it fits inan
        agile environment, I also have experience developing a lot of API's and
        create Web apps using micro-services, but I also enjoy digital
        fabrication, especially 3D printing, and I have some small projects for
        theIoT using physical computing.`}
      </p>
    </>
  );
};

export default CardBody;
