import React from "react";

interface Props {
  href: string;
  target?: string;
  children: React.ReactNode;
}

const Link: React.FunctionComponent<Props> = ({ href, children, ...rest }) => {
  return (
    <a className="link" href={href} {...rest}>
      {children}
    </a>
  );
};

export default Link;
