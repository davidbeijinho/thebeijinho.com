import Nav from "components/Nav/Nav";
import Footer from "components/Footer/Footer";
import Head from "next/head";

// TODO: wrapper div can be the body itsell
// <div className="bg-gray-100 font-sans leading-normal tracking-normal">
const Layout = ({ children }) => {
    return <>
        <Head>
            <html lang="en" />
            <title>David Beijinho</title>
            <link
                rel="apple-touch-icon"
                sizes="180x180"
                href="/apple-touch-icon.png"
            />
            <meta name="description" content="David Beijinho Web v0.1.0" />
            <link
                rel="icon"
                type="image/png"
                sizes="32x32"
                href="/favicon-32x32.png"
            />
            <link
                rel="icon"
                type="image/png"
                sizes="16x16"
                href="/favicon-16x16.png"
            />
            <link rel="manifest" href="/site.webmanifest" />
        </Head>
        <Nav />
        <div className="mt-9 pt-9   h-screen">
            {children}
        </div>
        <Footer />
    </>
}

export default Layout;