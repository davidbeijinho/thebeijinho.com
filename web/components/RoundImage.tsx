import React from "react";

// can this maybe be a <img tag ?
const RoundImage: React.FunctionComponent = () => {
  return (
    <div
      className="block lg:hidden rounded-full shadow-xl mx-auto -mt-16 h-48 w-48 bg-cover bg-center"
      style={{
        backgroundImage: "url('/images/me.webp')",
      }}
    ></div>
  );
};

export default RoundImage;
