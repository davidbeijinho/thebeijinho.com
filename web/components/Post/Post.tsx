import PostTitle from "components/PostTitle/PostTitle";
import PostContent from "components/PostContent/PostContent";

// style="font-family:Georgia,serif;"

const Post = () => {
	return <div className="w-full px-4 md:px-6 text-xl text-gray-800 leading-normal" >
		<PostTitle />
		<PostContent />
	</div>
}

export default Post;