import { gql, useQuery } from "@apollo/client";
import withApollo from "lib/apollo";
import Tags from "components/Tags/Tags";
import Post from "components/Post/Post";
import Nav from "components/Nav/Nav";
const POSTS_PER_PAGE = 10;

const GET_POSTS = gql`
  query allPostss($first: Int!, $skip: Int!){
    allPosts(first: $first, skip: $skip, sortBy:id_DESC) {
      id
      title
      content {
        document
      }
    }
    _allPostsMeta {
        count
    }
}

`;
1

const Blog: React.FunctionComponent = () => {

    const { loading, error, data, fetchMore } = useQuery(GET_POSTS, {
        variables: { skip: 0, first: POSTS_PER_PAGE },
        notifyOnNetworkStatusChange: true
    });

    if (error) {
        return <>Error: {JSON.stringify(error)}</>;
    }

    if (loading) {
        return <>loading...</>;
    }

    return (
        <>
            <h1>Posts:</h1>
            {data.allPosts.map(post => {
                return <div key={post.id}>
                    <h2>{post.title}</h2>
                    <p>{JSON.stringify(post.content.document)}</p>
                </div>
            })}
            <h3>Total posts: {data._allPostsMeta.count}</h3>
            <div className="bg-gray-100 font-sans leading-normal tracking-normal">
                <Nav />
                <div className="container w-full md:max-w-3xl mx-auto pt-20">
                    <Post />
                    <Tags />
                </div>
            </div>
        </>
    );
};

export default withApollo({ ssr: false })(Blog);
