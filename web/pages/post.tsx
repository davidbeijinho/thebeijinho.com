import Tags from "components/Tags/Tags";
import Post from "components/Post/Post";
import Layout from "components/Layout/Layout";
import Footer from "components/Footer/Footer";

const PostPage: React.FunctionComponent = () => {
    return (
        <Layout>
            <div className="container max-w-7xl mx-auto">
                <Post />
                <Tags />
            </div>
        </Layout>
    );
};

export default PostPage;
