import Grid_1_3 from "components/Grid/Grid_1_3";
import Grid_1_2 from "components/Grid/Grid_1_2";
import Grid_2_3 from "components/Grid/Grid_2_3";
import Layout from "components/Layout/Layout";

const Blog: React.FunctionComponent = () => {
	return (
		<Layout>
			<div className="container max-w-7xl flex flex-wrap mx-auto">
				<Grid_1_3 />
				<Grid_1_3 />
				<Grid_1_3 />
				<Grid_1_2 />
				<Grid_1_2 />
				<Grid_2_3 />
				<Grid_1_3 />
			</div>
		</Layout>
	);
};

export default Blog;
