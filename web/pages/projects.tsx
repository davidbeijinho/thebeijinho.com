import Grid_1_3 from "components/Grid/Grid_1_3";
import Layout from "components/Layout/Layout";

const Projects: React.FunctionComponent = () => {
	return (
		<Layout>
			<div className="container max-w-7xl flex flex-wrap mx-auto">
				<Grid_1_3 />
				<Grid_1_3 />
				<Grid_1_3 />
			</div>
		</Layout>
	);
};

export default Projects;
