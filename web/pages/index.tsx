import Head from "next/head";
import Profile from "components/Profile";
import Layout from "components/Layout/Layout";

const Home: React.FunctionComponent = () => {
  return (
    <Layout>
      <Head>
        <body
          className="bg-cover"
          style={{
            backgroundImage: "url('/images/background.webp')",
          }}
        />
      </Head>
      <Profile />
    </Layout>
  );
};

export default Home;
